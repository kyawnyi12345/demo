#init 
=======
FROM nginx

RUN rm -f /usr/share/nginx/html/index.html

COPY ./source/index.html /usr/share/nginx/html/index.html
COPY ./source/secondpage.html /usr/share/nginx/html/secondpage.html