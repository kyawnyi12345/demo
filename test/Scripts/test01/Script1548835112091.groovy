import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('')

WebUI.navigateToUrl('file:///Users/sbr6309923/kDocker/Demo-master/DEMO/source/index.html')

WebUI.setText(findTestObject('Object Repository/Page_/input_Name_name'), 'A')

WebUI.setText(findTestObject('Object Repository/Page_/input_Father Name_fname'), 'B')

WebUI.setText(findTestObject('Object Repository/Page_/input_Mother Name_mname'), 'C')

WebUI.click(findTestObject('Object Repository/Page_/button_Go to Next'))

WebUI.click(findTestObject('Object Repository/Page_/body_This is the second page'))

